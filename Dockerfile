FROM registry.gitlab.its.maine.edu/docker/node-base/14-oracle

WORKDIR /home/node/bin
COPY splunk.sh ./
COPY docker/docker-entrypoint.sh ./
RUN chmod +x ./*.sh

WORKDIR /home/node/app
COPY entities ./entities/
COPY libs ./libs/
RUN mkdir logs
COPY models ./models/
COPY src ./src/
COPY .env ./
COPY package.json ./
COPY yarn.lock ./

RUN yarn --non-interactive --no-progress --prod --silent

RUN chown node:node -R /home/node

USER node

ENTRYPOINT ["/home/node/bin/docker-entrypoint.sh"]
