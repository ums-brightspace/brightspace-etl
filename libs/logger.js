const winston = require("winston");

let console = new winston.transports.Console({
	level: "info",
	format: winston.format.combine(
		winston.format.timestamp(),
		winston.format.simple()
	),
});

if (process.env.NODE_ENV !== "production") {
	console.level = "debug";
	console.format = winston.format.combine(
		winston.format.timestamp(),
		winston.format.colorize(),
		winston.format.simple()
	);
}

let file = new winston.transports.File({
	filename: "logs/log.log",
	level: "info",
	format: winston.format.combine(
		winston.format.timestamp(),
		winston.format.json()
	),
});

const logger = winston.createLogger({
	exitOnError: false,
	transports: [
		console,
		new winston.transports.File({ filename: "logs/error.log", level: "emerg" }),
		file,
	],
	exceptionHandlers: [console],
	defaultMeta: { pid: process.pid },
	levels: winston.config.syslog.levels,
});

exports.default = logger;
