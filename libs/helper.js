const logger = require("../libs/logger").default;

exports.databaseCredentials = function () {
	let connString =
		process.env.DB_HOST + ":" + process.env.DB_PORT + "/" + process.env.DB_SID;
	let creds = {
		user: process.env.DB_USER,
		password: process.env.DB_PASSWORD,
		connectString: connString,
	};

	return creds;
};

exports.handleAxiosError = function (error) {
	let errObject = { Error: error.message };
	if (error.response) {
		errObject = {
			status: error.response.status,
			text: error.response.statusText,
			message: error.message,
		};
	} else if (error.request) {
		errObject = { Error: "No response received", message: error.message };
	}

	return errObject;
};

exports.segFaultHandler = async function (signal, address, stack) {
	logger.emerg("SegmentFaultHandler", {
		entity: "main",
		action: "system",
		signal,
		address,
		stack,
	});
	await sleep(3000); // sleep here in order for logs to get sent to splunk
};

function sleep(ms) {
	return new Promise(function (resolve) {
		setTimeout(resolve, ms);
	});
}
