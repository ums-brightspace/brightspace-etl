let Base = require("./base").default;

let User = Object.create(Base);

User.name = "User";
User.slug = "user";
User.endPoint = "user";
User.umsIdField = "EMPLID";
User.dbFields = [
	"USERNAME",
	"FIRST_NAME",
	"MIDDLE_NAME",
	"LAST_NAME",
	"EMPLID",
	"EMAIL",
];

User.makeLmsEntity = function (dbUser) {
	let data = {
		userId: dbUser.EMPLID,
		username: dbUser.USERNAME,
		email: dbUser.EMAIL,
		firstName: dbUser.FIRST_NAME,
		middleName: "",
		lastName: dbUser.LAST_NAME,
		campuses: [],
		primaryCampus: "",
	};

	if (dbUser.MIDDLE_NAME !== null) {
		data.middleName = dbUser.MIDDLE_NAME;
	}

	return data;
};

exports.default = User;
