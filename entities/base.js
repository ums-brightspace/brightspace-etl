const db = require("../libs/db");
const logger = require("../libs/logger").default;
const helper = require("../libs/helper");
const dbLog = require("../models/log");
const dayjs = require("dayjs");
const axios = require("../libs/axios").default();

exports.default = {
	name: "aaa",
	slug: function () {
		throw "Must provide a string slug";
	},
	endPoint: this.slug,
	addGeneratesNewId: true, // This will be true for all but enrollments, which are not true entities
	dbFields: function () {
		throw "Must provide an array of strings for database fields";
	},
	httpRequestTimeoutFactor: 1, // Used as a multiple of HTTP_REQUEST_TIMEOUT for entities that take a long time
	lmsToDbFieldMapping: function () {
		throw "Must provide a hash of string/string for mapping of LMS fields to database fields with the hash being the LMS field";
	},
	umsIdField: "CODE", // This is the db field that contains the primary key for the entity
	lmsViewName: function (queryType) {
		return "lms_" + this.slug + "s_" + queryType;
	},
	searchByUmsId: false, //For use when an entity uses something other than LMS_ID as the primary key
	successId: function (responseData) {
		return new Promise(function (resolve) {
			resolve(responseData.id);
		});
	},
	makeLmsEntity: function (dbEntity) {
		throw "Must provide a function that accepts the database entity and returns an object that will be passed to LMS ETL";
	},
	getEntityFromDbNew: function () {
		return this.getEntityFromDb("new");
	},
	getEntityFromDbUpdate: function () {
		return this.getEntityFromDb("updates");
	},
	getEntityFromDb: function (queryType) {
		let fields = this.dbFields.slice();
		if (
			queryType === "updates" &&
			this.slug !== "enrollment" &&
			this.slug !== "role"
		) {
			fields.push("lms_id");
		}

		let sql =
			"Select " + fields.join(",") + " From " + this.lmsViewName(queryType);

		return db.queryDb(sql);
	},
	processNewEntity: function (dbEntity) {
		let Entity = this;
		let lmsEntity = Entity.makeLmsEntity(dbEntity);
		let lmsId = "";

		return new Promise(function (resolve, reject) {
			axios
				.post("/" + Entity.endPoint, lmsEntity, {
					timeout:
						process.env.HTTP_REQUEST_TIMEOUT * Entity.httpRequestTimeoutFactor,
				})
				.catch(function (err) {
					logger.notice("Error adding " + Entity.name, {
						entity: Entity.slug,
						action: "post",
						error: helper.handleAxiosError(err),
					});
					throw new Error("Unable to add " + Entity.name);
				})
				.then(async function (res) {
					logger.info("Posted " + Entity.name + " Successfully", {
						entity: Entity.slug,
						action: "post",
						lmsEntity,
					});
					lmsId = await Entity.successId(res.data);
					return res;
				})
				.then(function logActionToDb(res) {
					dbLog.addLog(
						Entity.slug,
						dbEntity[Entity.umsIdField],
						lmsId,
						"post",
						res.status,
						res.statusText,
						res.data
					);

					return Entity.successId(res.data);
				})
				.then(function RecordEntityHasBeenAdded(lmsId) {
					return Entity.recordEntityAction(dbEntity, lmsId);
				})
				.catch(function (error) {
					logger.warning("Error on add", {
						entity: Entity.slug,
						action: "post",
						lmsEntity: lmsEntity,
						dbEntity: dbEntity,
						error: error,
					});
					return dbLog.addLog(
						Entity.slug,
						dbEntity[Entity.umsIdField],
						"",
						"post",
						null,
						"Errored on add",
						null
					);
				})
				.then(function () {
					resolve();
				});
		});
	},
	processUpdateEntity: function (dbEntity) {
		let Entity = this;
		let lmsEntity = Entity.makeLmsEntity(dbEntity);

		return new Promise(function (resolve, reject) {
			axios
				.put("/" + Entity.endPoint + "/" + dbEntity.LMS_ID, lmsEntity, {
					timeout:
						process.env.HTTP_REQUEST_TIMEOUT * Entity.httpRequestTimeoutFactor,
				})
				.catch(function (err) {
					logger.notice("Error updating " + Entity.name, {
						entity: Entity.slug,
						action: "put",
						error: helper.handleAxiosError(err),
					});
					throw new Error("Unable to update " + Entity.name);
				})
				.then(function (res) {
					logger.info("Put " + Entity.name + " Successfully", {
						entity: Entity.slug,
						action: "put",
						lmsEntity,
					});
					return res;
				})
				.then(function logActionToDb(res) {
					dbLog.addLog(
						Entity.slug,
						dbEntity[Entity.umsIdField],
						dbEntity.LMS_ID,
						"put",
						res.status,
						res.statusText,
						res.data
					);

					return Entity.successId(res.data);
				})
				.then(function RecordEntityHasBeenUpdated(lmsId) {
					return Entity.recordUpdatedEntity(dbEntity, lmsId);
				})
				.catch(function (error) {
					logger.warning("Error on update", {
						entity: Entity.slug,
						action: "put",
						lmsEntity: lmsEntity,
						dbEntity: dbEntity,
						error: error,
					});
					return dbLog.addLog(
						Entity.slug,
						dbEntity[Entity.umsIdField],
						dbEntity.LMS_ID,
						"put",
						null,
						"Errored on update",
						null
					);
				})
				.then(function () {
					resolve();
				});
		});
	},
	recordEntityAction: function (dbEntity, lmsId) {
		let Entity = this;

		return new Promise(function (resolve, reject) {
			Entity.isCompletedEntityPresent(dbEntity, lmsId)
				.then(function (isPresent) {
					if (isPresent) {
						return Entity.recordUpdatedEntity(dbEntity, lmsId);
					} else {
						return Entity.recordCompletedEntity(dbEntity, lmsId);
					}
				})
				.then(function (rowsAffected) {
					resolve(rowsAffected);
				})
				.catch(function (error) {
					reject(error);
				});
		});
	},
	isCompletedEntityPresent: function (dbEntity, lmsId) {
		let sql =
			"Select 1 From " + this.slug + "_completed " + " Where LMS_ID = :LMS_ID";
		let idValue = lmsId;
		if (this.searchByUmsId) {
			sql =
				"Select 1 From " +
				this.slug +
				"_completed " +
				" Where " +
				this.umsIdField +
				" = :" +
				this.umsIdField;
			idValue = dbEntity[this.umsIdField];
		}

		return new Promise(function (resolve, reject) {
			db.queryDb(sql, [idValue], false)
				.then(function (rows) {
					if (rows.length === 1) {
						resolve(true);
					} else {
						resolve(false);
					}
				})
				.catch(function (error) {
					reject(error);
				});
		});
	},
	recordCompletedEntity: function (dbEntity, lmsId) {
		let fields = this.dbFields.slice();
		let dt = dayjs().format("YYYY-MM-DD HH:mm:ss");
		let insertData = this.dbFields.map((f) => dbEntity[f]);

		fields.push("ADDED_ON");
		insertData.push(dt);
		fields.push("MODIFIED_ON");
		insertData.push(dt);
		if (this.addGeneratesNewId) {
			fields.push("LMS_ID");
			insertData.push(lmsId);
		}

		let fieldString = fields.join(", ");
		let valueString = ":" + fields.join(", :");

		let sql =
			"Insert into " +
			this.slug +
			"_completed (" +
			fieldString +
			") values(" +
			valueString +
			")";

		return db.executeSQL(sql, insertData, true);
	},
	recordUpdatedEntity: function (dbEntity, lmsId) {
		let fields = this.dbFields.slice();
		let dt = dayjs().format("YYYY-MM-DD HH:mm:ss");
		let data = this.dbFields.map((f) => dbEntity[f]);

		fields.push("MODIFIED_ON");
		data.push(dt);
		if (this.searchByUmsId) {
			data.push(dbEntity[this.umsIdField]);
		} else {
			data.push(lmsId);
		}

		let fieldString = "";
		fields.forEach(function (field) {
			fieldString += ", " + field + "= :" + field;
		});
		fieldString = fieldString.substring(1);

		let sql = "Update " + this.slug + "_completed Set " + fieldString;
		if (this.searchByUmsId) {
			sql += " Where " + this.umsIdField + " = :" + this.umsIdField;
		} else {
			sql += " Where LMS_ID = :LMS_ID";
		}
		return db.executeSQL(sql, data, true);
	},
};
