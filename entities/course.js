let Base = require("./base").default;

let Course = Object.create(Base);

Course.name = "Course";
Course.slug = "course";
Course.endPoint = "course";
Course.dbFields = [
	"CODE",
	"TITLE",
	"PARENTS",
	"CLASS_NUMBER",
	"INSTITUTION",
	"TERM",
	"IS_COMBINED",
	"SESSION_CODE",
	"MASTER",
	"SEMESTER",
	"START_DT",
	"END_DT",
	"IS_ACTIVE",
];

Course.makeLmsEntity = function (dbItem) {
	let data = {
		code: dbItem.CODE,
		name: dbItem.TITLE,
		parents: dbItem.PARENTS.split("|"),
		master: dbItem.MASTER,
		semester: dbItem.SEMESTER,
	};

	if (dbItem.IS_ACTIVE === 1) {
		data.isActive = true;
	}
	if (dbItem.IS_ACTIVE === 0) {
		data.isActive = false;
	}

	if (dbItem.START_DT !== null) {
		data.startDate = dbItem.START_DT;
	}

	if (dbItem.END_DT !== null) {
		data.endDate = dbItem.END_DT;
	}

	return data;
};

Course.getEntityFromDbUpdate = function () {
	return new Promise(function (resolve) {
		resolve([]);
	});
};

exports.default = Course;
