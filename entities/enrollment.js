let Base = require("./base").default;
const helper = require("../libs/helper");
const logger = require("../libs/logger").default;
const Log = require("../models/log");
const db = require("../libs/db");
const dayjs = require("dayjs");
const axios = require("../libs/axios").default();

let Enrollment = Object.create(Base);

Enrollment.name = "Enrollment";
Enrollment.slug = "enrollment";
Enrollment.endPoint = "enrollment";
Enrollment.addGeneratesNewId = false;
Enrollment.dbFields = ["USER_LMS_ID", "COURSE_LMS_ID", "TYPE", "ENROLL_STATUS"];
Enrollment.umsIdField = null;
Enrollment.successId = function (responseData) {
	return new Promise(function (resolve) {
		resolve(
			responseData.userId +
				"-" +
				responseData.courseId +
				":" +
				responseData.roleId
		);
	});
};

Enrollment.makeLmsEntity = function (dbEnroll) {
	return {
		courseLmsId: dbEnroll.COURSE_LMS_ID,
		userLmsId: dbEnroll.USER_LMS_ID,
		type: dbEnroll.TYPE,
		umsId: "",
		lmsId: dbEnroll.USER_LMS_ID + "-" + dbEnroll.COURSE_LMS_ID,
	};
};

Enrollment.processDelete = function (dbEntity) {
	let Entity = this;
	let lmsEntity = this.makeLmsEntity(dbEntity);

	return new Promise(function (resolve, reject) {
		axios
			.delete("/" + Entity.slug, {
				data: lmsEntity,
			})
			.catch(function (err) {
				if (
					err.response &&
					err.response.status === 400 &&
					err.response.data &&
					err.response.data.errors &&
					err.response.data.errors.status === 404
				) {
					logger.info("Enrollment not found, so not deleted ", {
						entity: Entity.slug,
						action: "delete",
						lmsEntity,
					});
					return err.response;
				} else {
					logger.notice("Error deleting " + Entity.name, {
						entity: Entity.slug,
						action: "delete",
						error: helper.handleAxiosError(err),
					});
				}
				throw new Error("Unable to delete " + Entity.name);
			})
			.then(function (res) {
				logger.info("Deleted " + Entity.name + " Successfully", {
					entity: Entity.slug,
					action: "delete",
					lmsEntity,
				});
				return res;
			})
			.then(function logActionToDb(res) {
				Log.addLog(
					Entity.slug,
					lmsEntity.umsId,
					lmsEntity.lmsId,
					"delete",
					res.status,
					res.statusText,
					res.data
				);

				return Entity.successId(res.data);
			})
			.then(function RecordEntityHasBeenDeleted(lmsId) {
				return Entity.recordEntityAction(dbEntity, lmsId);
			})
			.catch(function (error) {
				logger.warning("Error on delete", {
					entity: Entity.slug,
					action: "delete",
					lmsEntity: lmsEntity,
					dbEntity: dbEntity,
					error: error,
				});
				return Log.addLog(
					Entity.slug,
					lmsEntity.umsId,
					lmsEntity.lmsId,
					"delete",
					null,
					"Errored on delete",
					null
				);
			})
			.then(function () {
				resolve();
			});
	});
};

Enrollment.processNewEntity = function (dbEntity) {
	if (dbEntity.ENROLL_STATUS === "E") {
		return Base.processNewEntity.call(this, dbEntity);
	} else {
		return Enrollment.processDelete(dbEntity);
	}
};

Enrollment.processUpdateEntity = function (dbEntity) {
	if (dbEntity.ENROLL_STATUS === "E") {
		return Base.processNewEntity.call(this, dbEntity);
	} else {
		return Enrollment.processDelete(dbEntity);
	}
};

Enrollment.recordUpdatedEntity = function (dbEntity, lmsId) {
	let dbConn;
	let Entity = this;

	return new Promise(function (resolve, reject) {
		db.getConnection()
			.catch(function (err) {
				logger.error("Error getting connection to update a completed entity", {
					entity: Entity.slug,
					action: "database",
					error: err,
				});
				return reject("Error getting connection: " + err);
			})
			.then(function updateCompletedEntity(databaseConnection) {
				dbConn = databaseConnection;

				let dt = dayjs().format("YYYY-MM-DD HH:mm:ss");
				let data = [];
				data.push(dbEntity.TYPE);
				data.push(dbEntity.ENROLL_STATUS);
				data.push(dt);
				data.push(dbEntity.USER_LMS_ID);
				data.push(dbEntity.COURSE_LMS_ID);

				let sql =
					"Update " +
					Entity.slug +
					"_completed Set " +
					" TYPE = :TYPE, ENROLL_STATUS = :ENROLL_STATUS, MODIFIED_ON = :MODIFIED_ON" +
					" Where USER_LMS_ID = :USER_LMS_ID And COURSE_LMS_ID = :COURSE_LMS_ID";

				return dbConn.execute(sql, data, { autoCommit: true });
			})
			.then(function processResults(results) {
				resolve(results.rowsAffected);
			})
			.catch(function (error) {
				reject(error);
			})
			.finally(function closeDatabase() {
				dbConn.close();
			})
			.catch(function (err) {
				logger.notice("Unable to close database connection", {
					entity: Entity.slug,
					action: "database",
					error: err.message,
				});
			});
	});
};

Enrollment.isCompletedEntityPresent = function (dbEntity, lmsId) {
	let dbConn;
	let Entity = this;

	return new Promise(function (resolve, reject) {
		db.getConnection()
			.catch(function (err) {
				logger.error(
					"Error getting connection to check for enrollment existence",
					{
						entity: Entity.slug,
						action: "database",
						error: err,
					}
				);
				return reject("Error getting connection: " + err);
			})
			.then(function checkForEnrollment(databaseConnection) {
				dbConn = databaseConnection;

				let sql =
					"Select 1 From " +
					Entity.slug +
					"_completed " +
					" Where COURSE_LMS_ID = :COURSE_LMS_ID And USER_LMS_ID = :USER_LMS_ID";

				return dbConn.execute(sql, [
					dbEntity.COURSE_LMS_ID,
					dbEntity.USER_LMS_ID,
				]);
			})
			.then(function processResults(results) {
				if (results.rows.length === 1) {
					resolve(true);
				} else {
					resolve(false);
				}
			})
			.catch(function (error) {
				reject(error);
			})
			.finally(function closeDatabase() {
				dbConn.close();
			})
			.catch(function (err) {
				logger.notice("Unable to close database connection: ", {
					entity: Entity.slug,
					action: "database",
					error: err.message,
				});
			});
	});
};

exports.default = Enrollment;
