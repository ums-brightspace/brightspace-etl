# Brightspace ETL

[Software Artifact Page](https://gojira.its.maine.edu/confluence/display/WB/Brightspace+ETL)

[Brightspace](https://www.d2l.com/products/learning-environment/) Extract Transform Load App

This process will pull data from MaineStreet (UMS [PeopleSoft](https://www.oracle.com/applications/PEOPLESOFT/) implementation) and push it to [Brightspace REST API](https://gitlab.its.maine.edu/development/lms/brightspace-etl) / [Public Brightspace REST API](https://gitlab.com/calbis/brightspace-rest-api).

## Note to the Public

This is written and maintained for the use of the [University of Maine System](https://www.maine.edu/). Feature requests will likely not be acted on. Merge requests will be reviewed, but there is no guarantee that they will be accepted.

### Our Environment

We utilize a self hosted version of [Gitlab](https://about.gitlab.com/stages-devops-lifecycle/) Enterprise, backed by [Rancher](https://rancher.com/). Our CI is [Auto DevOps](https://about.gitlab.com/stages-devops-lifecycle/auto-devops/), which requires zero configuration to build and deploy the application to the kubernetes clusters. Git branching is in the standard [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). This repository that you are seeing is a [mirror](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html) of our private repository.

## Development

To setup processing a new entity, see `/entities/base.js`. Methods that have a throw clause will need to be filled in with the appropriate information for that entity. Also, a row will need to be inserted into entity_lock with the slug as the entity and 0 for the is_locked.

### Environment Variables

| Variable             | Description                                                                                                                  | Value                                                                                                                                                       |
| -------------------- | ---------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| DB\*\*               | Database credentials                                                                                                         | N/A                                                                                                                                                         |
| LMS_ETL_HOST         | Hostname of the [Brightspace REST API](https://gitlab.its.maine.edu/development/lms/brightspace-etl)                         | http://localhost.localdomain:5000                                                                                                                           |
| HTTP_REQUEST_TIMEOUT | Timeout in milliseconds                                                                                                      | 5000                                                                                                                                                        |
| JWT_TOKEN            | [JSON Web Token](https://jwt.io/) to connect to the REST API                                                                 | eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c |
| ETL_SCHEDULE         | once, to run one time only, or a [cron style schedule](https://github.com/node-schedule/node-schedule#cron-style-scheduling) | once                                                                                                                                                        |
| ENTITIES_TO_RUN      | all, to run all entities, or which entities to run as a comma delimited list with no spaces                                  | college,unit,sandbox                                                                                                                                        |
| CALLS_LIMIT          | How many concurrent calls to make at once to the REST API                                                                    | 20                                                                                                                                                          |

### Oracle Database

This app is intended to connect to an [Oracle database](https://www.oracle.com/database/). To do so, you will need an [Oracle Instant Client](https://www.oracle.com/database/technologies/instant-client/downloads.html).

## Logging

This app uses the syslog standard for logging levels. Below are examples of the types of messages for the types of situations the application will encounter.

### emerg

- System cannot connect to the database.
- Application received a termination signal.

### alert

- Entity tried to run while another has the lock.

### crit

- Whole entity not processed.
- Unable to update entity lock status.

### error

- Database connection issue on a single transaction.

### warning

- Database insert/update error.

### notice

- HTTP client errors.
- Error on closing a database connection.

### info

- Action succeeded.
- Entity processing state.
- General application state.

### debug

- General debug messages.
