require("dotenv").config();

let Enrollment = require("../entities/enrollment").default;
let User = require("../entities/user").default;
let Semester = require("../entities/semester").default;
let Sandbox = require("../entities/sandbox").default;
let College = require("../entities/college").default;
let AcademicUnit = require("../entities/unit").default;
let Course = require("../entities/course").default;
let Role = require("../entities/role").default;
const logger = require("../libs/logger").default;
let asyncPool = require("tiny-async-pool");
const db = require("../libs/db");
const EntityLock = require("../models/entity_lock");
const helper = require("../libs/helper");
let termsToProcess = require("../models/terms_to_process");

const segFaultHandler = require("segfault-handler");
segFaultHandler.registerHandler("../logs/crash.log", helper.segFaultHandler);

exports.main = function () {
	db.initDb()
		.catch(function (err) {
			logger.emerg("Unable to connect to the db", {
				entity: "main",
				action: "database",
				err,
			});
			process.exit(1);
		})
		.then(function () {
			return runEntities();
		});
};

if (require.main === module) {
	exports.main();
}

function runEntities() {
	logger.info("Start of processing all entities", {
		entity: "main",
		action: "system",
	});
	runEntity(Semester)
		.then(function () {
			return runEntity(College);
		})
		.then(function () {
			return runEntity(AcademicUnit);
		})
		.then(function () {
			return runEntity(User);
		})
		.then(function () {
			return runEntity(Sandbox);
		})
		.then(function () {
			return runEntity(Role);
		})
		.then(function () {
			return runTermLimitedEntities();
		})
		.then(function () {
			logger.info("Finished processing all entities", {
				entity: "main",
				action: "system",
			});
		})
		.then(function () {
			db.closeDb()
				.then(function () {
					logger.info("All database connections closed", {
						entity: "main",
						action: "system",
					});
				})
				.catch(function (err) {
					logger.info("Problem closing all database connections", {
						entity: "main",
						action: "database",
						err,
					});
				})
				.finally(function () {
					logger.info("Done", { entity: "main", action: "system" });
					process.exit(0);
				});
		})
		.catch(function (error) {
			logger.alert("Unknown error running entities ", {
				entity: "main",
				action: "system",
				error,
			});
			process.exit(1);
		});
}

function runTermLimitedEntities() {
	return new Promise(function (resolve) {
		let terms = [];
		termsToProcess
			.getTermsForProcessing()
			.then(function (termsForProcessing) {
				terms = termsForProcessing;
				return asyncPool(1, terms, function (dbItem) {
					return runTermLimitedEntity(Course, dbItem.TERM);
				});
			})
			.then(function () {
				return asyncPool(1, terms, function (dbItem) {
					return runTermLimitedEntity(Enrollment, dbItem.TERM);
				});
			})
			.catch(function (error) {
				logger.alert("Error running term limited entities ", {
					entity: "main",
					action: "system",
					error,
				});
			})
			.finally(function () {
				return resolve();
			});
	});
}

function runTermLimitedEntity(Entity, term) {
	return new Promise(function (resolve) {
		EntityLock.updateTermBeingProcesses(Entity, term)
			.catch(function (error) {
				logger.crit(Entity.name + " not processed due to error", {
					entity: Entity.slug,
					term: term,
					action: "system",
					error,
				});
			})
			.then(function () {
				return runEntity(Entity, term);
			})
			.finally(function () {
				resolve();
			});
	});
}

function runEntity(Entity, term) {
	if (!term || term === "undefined") {
		term = "";
	}
	const entitiesToRun = process.env.ENTITIES_TO_RUN;

	return new Promise(function (resolve, reject) {
		if (!entitiesToRun) {
			return Promise.reject({
				message: "ENTITIES_TO_RUN needs to be provided",
				entity: "main",
			});
		} else if (
			entitiesToRun !== "all" &&
			!entitiesToRun.includes(Entity.slug)
		) {
			logger.info("As requested, skipping " + Entity.name, {
				entity: Entity.slug,
				term: term,
				action: "skip",
			});
			return resolve();
		} else {
			logger.info(Entity.name + ": Start of processing", {
				entity: Entity.slug,
				action: "start",
				term: term,
			});

			EntityLock.lock(Entity)
				.catch(function (error) {
					throw new Error(error);
				})
				.then(function () {
					return runEntityNew(Entity, term);
				})
				.then(function () {
					return runEntityUpdate(Entity, term);
				})
				.then(function () {
					return EntityLock.release(Entity);
				})
				.then(function () {
					logger.info(Entity.name + ": End of processing", {
						entity: Entity.slug,
						action: "end",
						term,
					});
				})
				.catch(function (error) {
					logger.crit(Entity.name + " not processed due to error", {
						entity: Entity.slug,
						action: "system",
						term,
						error,
					});
				})
				.finally(function () {
					return resolve();
				});
		}
	});
}

function runEntityNew(Entity, term) {
	logger.info(Entity.name + ": Start of processing new", {
		entity: Entity.slug,
		action: "start",
		term,
	});
	return new Promise(function (resolve, reject) {
		Entity.getEntityFromDbNew()
			.catch(function (err) {
				throw new Error(
					Entity.name + ": Unable to get new items from database: " + err
				);
			})
			.then(function processAll(items) {
				return asyncPool(
					Number(process.env.CALLS_LIMIT),
					items,
					function (dbItem) {
						return Entity.processNewEntity(dbItem);
					}
				);
			})
			.then(function () {
				logger.info(Entity.name + ": Finished processing new", {
					entity: Entity.slug,
					action: "end",
					term,
				});
				return resolve();
			})
			.catch(function (error) {
				logger.crit(Entity.name + ": Error mapping new ", {
					entity: Entity.slug,
					action: "system",
					term,
					error,
				});
				return resolve();
			});
	});
}

function runEntityUpdate(Entity, term) {
	logger.info(Entity.name + ": Start of processing updates", {
		entity: Entity.slug,
		action: "start",
		term,
	});
	return new Promise(function (resolve, reject) {
		Entity.getEntityFromDbUpdate()
			.catch(function (err) {
				throw new Error(
					Entity.name + ": Unable to get items to update from database: " + err
				);
			})
			.then(function processAll(items) {
				return asyncPool(
					Number(process.env.CALLS_LIMIT),
					items,
					function (dbItem) {
						return Entity.processUpdateEntity(dbItem);
					}
				);
			})
			.then(function () {
				logger.info(Entity.name + ": Finished processing updates", {
					entity: Entity.slug,
					action: "end",
					term,
				});
				return resolve();
			})
			.catch(function (error) {
				let err = error;
				if (error.message) {
					err = error.message;
				}
				logger.crit(Entity.name + ": Error mapping updates ", {
					entity: Entity.slug,
					action: "system",
					term,
					error: err,
				});
				return resolve();
			});
	});
}

process.once("SIGTERM", function () {
	return terminate("SIGTERM");
});
process.once("SIGINT", function () {
	return terminate("SIGINT");
});

function terminate(signal) {
	logger.crit(`Received ${signal} Termination Signal`, {
		entity: "main",
		action: "system",
		process: "runEntities",
		signal,
	});

	db.closeDb()
		.then(function () {
			logger.info("Terminate: Database closed", {
				entity: "main",
				action: "system",
				process: "runEntities",
			});
			process.exit(0);
		})
		.catch(function (err) {
			logger.error("Terminate: Unable to close the database ", {
				entity: "main",
				error: err,
				action: "system",
				process: "runEntities",
			});
			process.exit(1);
		});
}
