require("dotenv").config();

const logger = require("../libs/logger").default;
const cron = require("node-schedule");
const db = require("../libs/db");
const EntityLock = require("../models/entity_lock");
const { spawn } = require("child_process");
const helper = require("../libs/helper");
const treeKill = require("tree-kill");
const pidTree = require("pidtree");

const segFaultHandler = require("segfault-handler");
segFaultHandler.registerHandler("../logs/crash.log", helper.segFaultHandler);

exports.main = function () {
	runEntities();

	runDataSetup();

	runCourseAlerts();

	runOldIncompletes();
};

function runDataSetup() {
	const dataSetupSchedule = process.env.DATA_SETUP_SCHEDULE;
	const metaData = {
		entity: "main",
		action: "system",
		process: "runDataSetup",
	};

	if (dataSetupSchedule === "never" || dataSetupSchedule === "") {
		logger.alert("Running data setup job disabled", metaData);
	} else if (dataSetupSchedule === "once") {
		logger.alert("Running data setup job once", metaData);
		spawnProcess("runDataSetup");
	} else {
		logger.alert("Running job on schedule: " + dataSetupSchedule, metaData);
		cron.scheduleJob(dataSetupSchedule, function () {
			spawnProcess("runDataSetup");
		});
	}
}

function runCourseAlerts() {
	const schedule = process.env.COURSE_ALERTS_SCHEDULE;
	const metaData = {
		entity: "main",
		action: "system",
		process: "runCourseAlerts",
	};

	if (schedule === "never" || schedule === "") {
		logger.alert("Running course alert job disabled", metaData);
	} else if (schedule === "once") {
		logger.alert("Running course alert job once", metaData);
		spawnProcess("runCourseAlerts");
	} else {
		logger.alert("Running job on schedule: " + schedule, metaData);
		cron.scheduleJob(schedule, function () {
			spawnProcess("runCourseAlerts");
		});
	}
}

function runOldIncompletes() {
	const schedule = process.env.OLD_INCOMPLETES_SCHEDULE;
	const metaData = {
		entity: "main",
		action: "system",
		process: "runOldIncompletes",
	};

	if (schedule === "never" || schedule === "") {
		logger.alert("Running old incompletes job disabled", metaData);
	} else if (schedule === "once") {
		logger.alert("Running old incompletes job once", metaData);
		spawnProcess("runOldIncompletes");
	} else {
		logger.alert("Running job on schedule: " + schedule, metaData);
		cron.scheduleJob(schedule, function () {
			spawnProcess("runOldIncompletes");
		});
	}
}

function runEntities() {
	const etlSchedule = process.env.ETL_SCHEDULE;
	if (etlSchedule === "never" || etlSchedule === "") {
		logger.alert("Running entity job disabled", {
			entity: "main",
			action: "system",
			process: "runEntities",
		});
		return;
	}

	db.initDb()
		.catch(function (err) {
			logger.emerg("Unable to connect to the db", {
				entity: "main",
				action: "database",
				process: "runEntities",
				error: err,
			});
			process.exit(1);
		})
		.then(function ResetEntityLocks() {
			if (etlSchedule === "once") {
				return Promise.resolve(0);
			} else {
				return EntityLock.releaseAll();
			}
		})
		.then(function (rows) {
			logger.info(
				"Startup-Release Entity Locks: Unlocked " + rows + " Entities",
				{
					entity: "main",
					action: "system",
					process: "runEntities",
				}
			);
		})
		.then(function () {
			if (etlSchedule === "once") {
				logger.alert("Running entity jobs once", {
					entity: "main",
					action: "system",
					process: "runEntities",
				});
				spawnProcess("runEntities");
			} else {
				logger.alert("Running entity job on schedule: " + etlSchedule, {
					entity: "main",
					action: "system",
					process: "runEntities",
				});
				cron.scheduleJob(etlSchedule, function () {
					spawnProcess("runEntities");
				});
			}
		});
}

function spawnProcess(scriptName) {
	let proc = spawn("node", ["./src/" + scriptName + ".js"], {
		stdio: "inherit",
	});
	logger.info("Spawning " + scriptName + ", running under PID " + proc.pid, {
		pid: proc.pid,
		entity: "main",
		action: "system",
		process: scriptName,
	});

	const basicMetaData = {
		pid: proc.pid,
		entity: "main",
		action: "system",
		process: scriptName,
	};

	proc.on("exit", function (code, signal) {
		if (signal === null) {
			if (code > 0) {
				logger.crit(
					`Spawned process, ${proc.pid}, exited with code ${code}`,
					basicMetaData
				);
			} else {
				logger.info(
					`Spawned process, ${proc.pid}, exited with code ${code}`,
					basicMetaData
				);
			}
		} else {
			logger.crit(
				`Spawned process, ${proc.pid}, ${signal} termination received`,
				basicMetaData
			);
		}
	});

	proc.on("close", function () {
		logger.info("Child closed", basicMetaData);
	});
}

if (require.main === module) {
	exports.main();
}

process.once("SIGTERM", function () {
	return terminate("SIGTERM");
});
process.once("SIGINT", function () {
	return terminate("SIGINT");
});

function terminate(signal) {
	logger.emerg(`Received ${signal} Termination Signal`, {
		entity: "main",
		action: "system",
	});

	pidTree(process.pid)
		.then(function (pids) {
			pids.forEach(function (pid) {
				logger.info("Terminating child", {
					entity: "main",
					action: "system",
					pid,
				});

				treeKill(pid, "SIGTERM", function (error) {
					logger.info("Terminated child", {
						entity: "main",
						action: "system",
						error,
						pid,
					});
				});
			});
		})
		.then(function () {
			return EntityLock.releaseAll();
		})
		.then(function (rows) {
			logger.info("Terminate: Unlocked " + rows + " Entities", {
				entity: "main",
				action: "system",
			});
		})
		.catch(function (err) {
			logger.alert("Terminate: Unable to release entities", {
				entity: "main",
				error: err,
				action: "system",
			});
		})
		.then(function CloseDatabase() {
			return db.closeDb();
		})
		.then(function () {
			logger.info("Terminate: Database closed", {
				entity: "main",
				action: "system",
			});
		})
		.catch(function (err) {
			logger.error("Terminate: Unable to close the database ", {
				entity: "main",
				error: err,
				action: "system",
			});
		})
		.finally(function () {
			treeKill(process.pid);
		});
}
