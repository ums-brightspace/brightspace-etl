require("dotenv").config();

const logger = require("../libs/logger").default;
const db = require("../libs/db");
const helper = require("../libs/helper");

const segFaultHandler = require("segfault-handler");
segFaultHandler.registerHandler("../logs/crash.log", helper.segFaultHandler);

exports.main = function () {
	db.initDb()
		.catch(function (err) {
			logger.emerg("Unable to connect to the db", {
				entity: "main",
				action: "database",
				process: "runDataSetup",
				err,
			});
			process.exit(1);
		})
		.then(function () {
			return run();
		});
};

if (require.main === module) {
	exports.main();
}

function run() {
	const metaData = {
		entity: "main",
		action: "system",
		process: "runDataSetup",
	};
	logger.info("Start of data setup", metaData);

	db.executeSQL("call ADD_NEW_COURSE_ORG_AS_UNKNOWN()")
		.then(function () {
			logger.info(
				"Created any new course organization associations, if needed",
				{
					...metaData,
					result: "success",
				}
			);
		})
		.then(function () {
			db.closeDb()
				.then(function () {
					logger.info("All database connections closed", metaData);
				})
				.catch(function (err) {
					logger.info("Problem closing all database connections", {
						...metaData,
						action: "database",
						err,
					});
				})
				.then(function () {
					logger.info("Done", metaData);
					process.exit(0);
				});
		})
		.catch(function (error) {
			logger.warning("Unknown error on data setup ", {
				...metaData,
				error,
				result: "error",
			});
			process.exit(1);
		});
}

process.once("SIGTERM", function () {
	return terminate("SIGTERM");
});
process.once("SIGINT", function () {
	return terminate("SIGINT");
});

function terminate(signal) {
	logger.crit(`Received ${signal} Termination Signal`, {
		entity: "main",
		action: "system",
		process: "runDataSetup",
		signal,
	});

	db.closeDb()
		.then(function () {
			logger.info("Terminate: Database closed", {
				entity: "main",
				action: "system",
				process: "runDataSetup",
			});
			process.exit(0);
		})
		.catch(function (err) {
			logger.error("Terminate: Unable to close the database ", {
				entity: "main",
				error: err,
				action: "system",
				process: "runDataSetup",
			});
			process.exit(1);
		});
}
